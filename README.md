# README #

For run this proyect you only need to download this repository in your apache html folder. 

### What is this repository for? ###

* This repositoy is DegustaBox test, is Time Tracker. You can add and update tasks.
* Version 1.0

### How do I get set up? ###

* You only need an Apache Server.
* If you need to update libraries, execute ``./composer.phar update`` in proyect folder.
* Database configuration: The data storage is an json file, locate in src/infrastructure/tasks.json


### You can use commands to start/end task or listing all tasks ###

* START/END Task => ``php bin/console time-tracker:manage-task --taskName="task name" --action="start or end"``
* List all tasks => ``php bin/console time-tracker:list-tasks``
