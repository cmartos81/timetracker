<?php

namespace TimeTracker\Domain\Task;

interface TaskRepository
{
    /** @return array | Task[] */
    public function getAll(): array;

    public function get(string $taskName): ?Task;

    public function persist(Task $task): void;
}