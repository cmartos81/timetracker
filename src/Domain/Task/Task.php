<?php

namespace TimeTracker\Domain\Task;

use DateTime;

class Task implements \JsonSerializable
{
    private string $name;
    private DateTime $startTime;
    private ?DateTime $finishTime;

    public function __construct(string $name, DateTime $startTime)
    {
        $this->name = $name;
        $this->startTime = $startTime;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function startTime(): DateTime
    {
        return $this->startTime;
    }

    public function finishTime(): ?DateTime
    {
        if (!isset($this->finishTime)) {
            return null;
        }

        return $this->finishTime;
    }

    public function setFinishTime(DateTime $finishTime): self
    {
        $this->finishTime = $finishTime;

        return $this;
    }

    public function markAsFinished(): void
    {
        $this->finishTime = new DateTime();
    }

    public function isFinished(): bool
    {
        return isset($this->finishTime);
    }

    public function duration(): string
    {
        $finishTime = $this->finishTime ?? new \DateTime();

        $interval = date_diff($this->startTime, $finishTime);

        return $interval->format('%h Hours %i Minute %s Seconds');
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}