<?php

namespace TimeTracker\Domain\Task;

class TaskNotFoundException extends \Exception
{
}