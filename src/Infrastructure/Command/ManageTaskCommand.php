<?php
namespace TimeTracker\Infrastructure\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TimeTracker\Application\Task\CreateNewTaskUseCase;
use TimeTracker\Application\UseCaseRequest;
use TimeTracker\Domain\Task\Task;
use TimeTracker\Domain\Task\TaskRepository;

class ManageTaskCommand extends Command
{
    private const START = 'start';
    private const END = 'end';

    protected static $name = 'time-tracker:manage-task';
    private static array $validActions = [self::START, self::END];

    private OutputInterface $output;
    private TaskRepository $taskRepository;
    private CreateNewTaskUseCase $createNewTaskUseCase;

    public function __construct(
        TaskRepository $taskRepository,
        CreateNewTaskUseCase $createNewTaskUseCase
    ) {
        parent::__construct(self::$name);

        $this->taskRepository = $taskRepository;
        $this->createNewTaskUseCase = $createNewTaskUseCase;
    }

    protected function configure(): void
    {
        $this
            ->setName(self::$name)
            ->addOption('taskName', 't', InputOption::VALUE_REQUIRED, 'Task Name')
            ->addOption('action', 'a', InputOption::VALUE_REQUIRED, 'start/end');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $taskName = $input->getOption('taskName');
        $action = $input->getOption('action');

        $this->output = $output;

        if (!in_array($action, self::$validActions)) {
            $output->writeln('No valid action!');

            return Command::SUCCESS;
        }

        $output->writeln(sprintf('Task Name: %s', $taskName));

        switch ($action) {
            case self::START:
                $this->startTask($taskName);
                break;
            case self::END:
                $this->endTask($taskName);
        }

        return Command::SUCCESS;
    }

    private function startTask(?string $taskName)
    {
        $task = $this->taskRepository->get($taskName);

        if (is_null($task)) {
            $response = $this->createNewTaskUseCase->execute(new UseCaseRequest([
                CreateNewTaskUseCase::PARAM_TASK_NAME => $taskName
            ]));

            if ($response->isError()) {
                $this->output->writeln('Error creating new task');
            }

            return;
        }

        $this->output->writeln(sprintf('The task %s has been running %s', $taskName, $task->duration()));
    }

    private function endTask(?string $taskName)
    {
        $task = $this->taskRepository->get($taskName);

        if (is_null($task)) {
            $this->output->writeln('Error not existing task!');

            return;
        }

        $task->markAsFinished();
        $this->taskRepository->persist($task);

        $this->output->writeln(sprintf('TASK ENDED!', $taskName));
    }
}