<?php

namespace TimeTracker\Infrastructure\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TimeTracker\Domain\Task\TaskRepository;

class ListTasksCommand extends Command
{
    protected static $name = 'time-tracker:list-tasks';

    private TaskRepository $taskRepository;

    public function __construct(TaskRepository $taskRepository)
    {
        parent::__construct(self::$name);

        $this->taskRepository = $taskRepository;
    }

    protected function configure(): void
    {
        $this->setName(self::$name);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('LISTING TASKS');

        $tasks = $this->taskRepository->getAll();
        foreach ($tasks as $task) {
            $output->writeln(sprintf(
                'NAME: %s | START TIME: %s | FINISH TIME: %s | STATUS: %s',
                $task->name(),
                $task->startTime()->format('d/m/Y h:i:s'),
                $task->finishTime() ? $task->finishTime()->format('d/m/Y h:i:s') : '?',
                $task->isFinished() ? 'FINISHED' : 'STILL RUNNING'
            ));
        }

        return Command::SUCCESS;
    }
}