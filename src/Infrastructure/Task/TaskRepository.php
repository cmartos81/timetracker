<?php

namespace TimeTracker\Infrastructure\Task;

use DateTime;
use TimeTracker\Domain\Task\Task;
use TimeTracker\Domain\Task\TaskRepository as TaskRepositoryInterface;

class TaskRepository implements TaskRepositoryInterface
{
    private const JSON_FILE = __DIR__ . '/tasks.json';

    /** @var array | Task[] $tasks */
    private array $tasks;

    public function __construct()
    {
        $json = file_get_contents(self::JSON_FILE);

        $rows = json_decode($json, true);

        foreach ($rows as $row) {
            $this->tasks[] = $this->hydrate($row);
        }
    }

    public function getAll(): array
    {
        return $this->tasks;
    }

    public function get(string $taskName): ?Task
    {
        foreach ($this->tasks as $task) {
            if ($task->name() === $taskName) {
                return $task;
            }
        }

        return null;
    }

    public function persist(Task $task): void
    {
        $this->tasks[] = $task;

        file_put_contents(self::JSON_FILE, json_encode($this->tasks));
    }

    /**
     * @param array $row
     *
     * @return Task
     *
     * @throws \Exception
     */
    private function hydrate($row): Task
    {
        $task = new Task(
            $row['name'],
            new DateTime($row['startTime']['date'])
        );

        if (isset($row['finishTime'])) {
            $task->setFinishTime(new DateTime($row['finishTime']['date']));
        }

        return $task;
    }
}