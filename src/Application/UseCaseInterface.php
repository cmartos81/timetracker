<?php

namespace TimeTracker\Application;

use TimeTracker\Application\UseCaseRequest;
use TimeTracker\Application\UseCaseResponse;

interface UseCaseInterface
{
    public function execute(UseCaseRequest $request): UseCaseResponse;
}