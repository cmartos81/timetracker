<?php

namespace TimeTracker\Application;

class UseCaseResponse extends UseCaseMessage
{
    public const NO_ERROR = 0;
    public const ERROR = 1;

    private int $errorCode;

    public function __construct($values = [])
    {
        parent::__construct($values);

        $this->errorCode = self::NO_ERROR;
    }

    public function isError(): bool
    {
        return $this->errorCode != self::NO_ERROR;
    }

    public function markAsError(): void
    {
        $this->errorCode = self::ERROR;
    }
}