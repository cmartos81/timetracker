<?php

namespace TimeTracker\Application\Task;

use TimeTracker\Application\UseCaseInterface;
use TimeTracker\Application\UseCaseRequest;
use TimeTracker\Application\UseCaseResponse;
use TimeTracker\Domain\Task\TaskNotFoundException;
use TimeTracker\Domain\Task\TaskRepository;

class FinishTaskUseCase implements UseCaseInterface
{
    public const PARAM_TASK_NAME = 'taskName';

    private TaskFinder $taskFinder;
    private TaskRepository $taskRepository;

    /**
     * @param TaskFinder $taskFinder
     * @param TaskRepository $taskRepository
     */
    public function __construct(TaskFinder $taskFinder, TaskRepository $taskRepository)
    {
        $this->taskFinder = $taskFinder;
        $this->taskRepository = $taskRepository;
    }

    public function execute(UseCaseRequest $request): UseCaseResponse
    {
        $response = new UseCaseResponse();

        try {
            $task = $this->taskFinder->find(
                $request->getValue(self::PARAM_TASK_NAME)
            );
        } catch (TaskNotFoundException $exception) {
            $response->markAsError();

            return $response;
        }

        $task->markAsFinished();
        $this->taskRepository->persist($task);

        return $response;
    }
}