<?php

namespace TimeTracker\Application\Task;

use TimeTracker\Domain\Task\Task;
use TimeTracker\Domain\Task\TaskNotFoundException;
use TimeTracker\Domain\Task\TaskRepository;

class TaskFinder
{
    private TaskRepository $taskRepository;

    /**
     * @param TaskRepository $taskRepository
     */
    public function __construct(TaskRepository $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

    /**
     * @throws TaskNotFoundException
     */
    public function find(string $taskName): Task
    {
        $task = $this->taskRepository->get($taskName);

        if (is_null($task)) {
            throw new TaskNotFoundException();
        }

        return $task;
    }
}