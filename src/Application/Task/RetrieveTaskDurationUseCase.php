<?php

namespace TimeTracker\Application\Task;

use TimeTracker\Application\UseCaseInterface;
use TimeTracker\Application\UseCaseRequest;
use TimeTracker\Application\UseCaseResponse;
use TimeTracker\Domain\Task\TaskNotFoundException;

class RetrieveTaskDurationUseCase implements UseCaseInterface
{
    public const PARAM_TASK_NAME = 'taskName';

    private TaskFinder $taskFinder;

    /**
     * @param TaskFinder $taskFinder
     */
    public function __construct(TaskFinder $taskFinder)
    {
        $this->taskFinder = $taskFinder;
    }

    public function execute(UseCaseRequest $request): UseCaseResponse
    {
        $response = new UseCaseResponse();

        try {
            $task = $this->taskFinder->find(
                $request->getValue(self::PARAM_TASK_NAME)
            );
        } catch (TaskNotFoundException $exception) {
            $response->markAsError();

            return $response;
        }

        $response->setValue('taskDuration', $task->duration());

        return $response;
    }
}