<?php

namespace TimeTracker\Application\Task;

use TimeTracker\Domain\Task\Task;

class EditTaskPageDto
{
    private string $taskName;
    private string $startTime;
    private string $duration;
    private bool $isFinished = false;
    private array $otherTasks = [];

    public function taskName(): string
    {
        return $this->taskName;
    }

    public function setTaskName(string $taskName): self
    {
        $this->taskName = $taskName;

        return $this;
    }

    public function startTime(): string
    {
        return $this->startTime;
    }

    public function setStartTime(string $startTime): self
    {
        $this->startTime = $startTime;

        return $this;
    }

    public function duration(): string
    {
        return $this->duration;
    }

    public function setDuration(string $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function isFinished(): bool
    {
        return $this->isFinished;
    }

    public function markAsFinished(): self
    {
        $this->isFinished = true;

        return $this;
    }

    public function addOtherTask(Task $task): void
    {
        $this->otherTasks[] = [
            'name' => $task->name(),
            'duration' => $task->duration(),
            'startDate' => $task->startTime()->format('d/m/Y h:i:s'),
            'isFinished' => $task->isFinished(),
        ];
    }

    public function otherTasks(): array
    {
        return $this->otherTasks;
    }
}