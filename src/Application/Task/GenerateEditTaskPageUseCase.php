<?php

namespace TimeTracker\Application\Task;

use TimeTracker\Application\UseCaseInterface;
use TimeTracker\Application\UseCaseRequest;
use TimeTracker\Application\UseCaseResponse;
use TimeTracker\Domain\Task\TaskNotFoundException;
use TimeTracker\Domain\Task\TaskRepository;

class GenerateEditTaskPageUseCase implements UseCaseInterface
{
    public const PARAM_TASK_NAME = 'taskName';

    private TaskFinder $taskFinder;
    private TaskRepository $taskRepository;

    /**
     * @param TaskFinder $taskFinder
     */
    public function __construct(TaskFinder $taskFinder, TaskRepository $taskRepository)
    {
        $this->taskFinder = $taskFinder;
        $this->taskRepository = $taskRepository;
    }

    public function execute(UseCaseRequest $request): UseCaseResponse
    {
        $response = new UseCaseResponse();

        try {
            $task = $this->taskFinder->find(
                $request->getValue(self::PARAM_TASK_NAME)
            );
        } catch (TaskNotFoundException $exception) {
            $response->markAsError();

            return $response;
        }

        $editTaskPageDto = new EditTaskPageDto();
        $editTaskPageDto
            ->setTaskName($task->name())
            ->setStartTime($task->startTime()->format('d/m/Y h:i:s'))
            ->setDuration($task->duration());

        if ($task->isFinished()) {
            $editTaskPageDto->markAsFinished();
        }

        $this->retrieveOtherTasks($editTaskPageDto);

        $response->setValue('editTaskPageDto', $editTaskPageDto);

        return $response;
    }

    private function retrieveOtherTasks(EditTaskPageDto $editTaskPageDto): void
    {
        $tasks = $this->taskRepository->getAll();

        foreach ($tasks as $task) {
            $editTaskPageDto->addOtherTask($task);
        }
    }
}