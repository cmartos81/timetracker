<?php

namespace TimeTracker\Application\Task;

use Symfony\Component\HttpFoundation\Request;
use TimeTracker\Application\UseCaseRequest;
use TimeTracker\Application\UseCaseResponse;
use DateTime;
use TimeTracker\Domain\Task\Task;
use TimeTracker\Domain\Task\TaskRepository;
use TimeTracker\Application\UseCaseInterface;

class CreateNewTaskUseCase implements UseCaseInterface
{
    public const PARAM_TASK_NAME = 'taskName';

    private TaskRepository $taskRepository;

    /**
     * @param TaskRepository $taskRepository
     */
    public function __construct(TaskRepository $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

    /**
     * @param UseCaseRequest $request
     * @return UseCaseResponse
     */
    public function execute(UseCaseRequest $request): UseCaseResponse
    {
        $response = new UseCaseResponse();

        $taskName = $request->getValue(self::PARAM_TASK_NAME);

        if (empty($taskName)) {
            $response->markAsError();
        }

        $task = new Task($taskName, new DateTime());

        $this->taskRepository->persist($task);

        $response
            ->setValue('taskName', $task->name())
            ->setValue('startTime', $task->startTime()->format('h:i:s'));

        return $response;
    }
}