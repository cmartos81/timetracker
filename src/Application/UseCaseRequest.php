<?php

namespace TimeTracker\Application;

class UseCaseRequest extends UseCaseMessage
{
    public function __construct($values = [])
    {
        parent::__construct($values);
    }
}