<?php

namespace TimeTracker\Application;

abstract class UseCaseMessage
{
    private array $values;

    public function __construct($values = [])
    {
        $this->values = $values;
    }

    public function hasValue($name)
    {
        return isset($this->values[$name]);
    }

    public function setValue($name, $value)
    {
        $this->values[$name] = $value;

        return $this;
    }

    public function getValue($name, $defaultValue = null)
    {
        return $this->hasValue($name) ? $this->values[$name] : $defaultValue;
    }

    public function getValues()
    {
        return $this->values;
    }

    public function jsonSerialize()
    {
        return $this->getValues();
    }
}