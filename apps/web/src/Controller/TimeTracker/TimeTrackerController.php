<?php

namespace App\Controller\TimeTracker;

use TimeTracker\Application\Task\CreateNewTaskUseCase;
use TimeTracker\Application\Task\EditTaskPageDto;
use TimeTracker\Application\Task\FinishTaskUseCase;
use TimeTracker\Application\Task\GenerateEditTaskPageUseCase;
use TimeTracker\Application\Task\RetrieveTaskDurationUseCase;
use TimeTracker\Application\Task\TaskFinder;
use TimeTracker\Application\UseCaseRequest;
use TimeTracker\Domain\Task\TaskNotFoundException;
use TimeTracker\Domain\Task\TaskRepository;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class TimeTrackerController extends AbstractController
{
    /**
     * @Route("/index", name="index")
     *
     * @param Request $request
     * @param TaskFinder $taskFinder
     *
     * @return Response
     */
    public function index(Request $request, TaskFinder $taskFinder): Response
    {
        $taskName = $request->get('taskName');

        if (empty($taskName)) {
            return $this->render('add_task.html.twig', [
                'taskName' => ''
            ]);
        }

        try{
            $taskFinder->find($taskName);
        } catch (TaskNotFoundException $exception) {
            return $this->forward('App\Controller\TimeTracker\TimeTrackerController::addTask', [
                'taskName'  => $taskName
            ]);
        }

        return $this->forward('App\Controller\TimeTracker\TimeTrackerController::editTask', [
            'taskName'  => $taskName
        ]);
    }

    /**
     * @Route("/add_task", name="add_task")
     *
     * @param string $taskName
     * @param CreateNewTaskUseCase $createNewTaskUseCase
     * @param GenerateEditTaskPageUseCase $generateEditTaskPageUseCase
     *
     * @return Response
     */
    public function addTask(
        string $taskName,
        CreateNewTaskUseCase $createNewTaskUseCase,
        GenerateEditTaskPageUseCase $generateEditTaskPageUseCase
    ): Response {
        $response = $createNewTaskUseCase->execute(new UseCaseRequest([
            CreateNewTaskUseCase::PARAM_TASK_NAME => $taskName
        ]));

        if ($response->isError()) {
            return $this->render('add_task.html.twig', [
                'taskName' => ''
            ]);
        }

        $response = $generateEditTaskPageUseCase->execute(new UseCaseRequest([
            GenerateEditTaskPageUseCase::PARAM_TASK_NAME => $taskName
        ]));

        /** @var EditTaskPageDto $editTaskPageDto */
        $editTaskPageDto = $response->getValue('editTaskPageDto');

        return $this->render('edit_task.html.twig', [
            'editTaskPageDto' => $editTaskPageDto
        ]);
    }

    /**
     * @Route("/edit_task", name="edit_task")
     *
     * @param Request $request
     * @param GenerateEditTaskPageUseCase $generateEditTaskPageUseCase
     *
     * @return Response
     */
    public function editTask(Request $request, GenerateEditTaskPageUseCase $generateEditTaskPageUseCase): Response
    {
        $response = $generateEditTaskPageUseCase->execute(new UseCaseRequest([
            GenerateEditTaskPageUseCase::PARAM_TASK_NAME => $request->get('taskName')
        ]));

        /** @var EditTaskPageDto $editTaskPageDto */
        $editTaskPageDto = $response->getValue('editTaskPageDto');

        return $this->render('edit_task.html.twig', [
            'editTaskPageDto' => $editTaskPageDto
        ]);
    }

    /**
     * @Route("/retrieve_duration", name="retrieve_duration")
     *
     * @param Request $request
     * @param RetrieveTaskDurationUseCase $retrieveTaskDurationUseCase
     *
     * @return Response
     */
    public function retrieveDuration(Request $request, RetrieveTaskDurationUseCase $retrieveTaskDurationUseCase): Response
    {
        $response = $retrieveTaskDurationUseCase->execute(new UseCaseRequest([
            RetrieveTaskDurationUseCase::PARAM_TASK_NAME => $request->get('taskName')
        ]));

        return new Response($response->getValue('taskDuration'));
    }

    /**
     * @Route("/finish_task", name="finish_task")
     *
     * @param Request $request
     * @param FinishTaskUseCase $finishTaskUseCase
     *
     * @return Response
     */
    public function finishTask(Request $request, FinishTaskUseCase $finishTaskUseCase): Response
    {
        $response = $finishTaskUseCase->execute(new UseCaseRequest([
            FinishTaskUseCase::PARAM_TASK_NAME => $request->get('taskName')
        ]));

        if ($response->isError()) {
            return new Response('ERROR', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new Response('OK', Response::HTTP_OK);
    }
}