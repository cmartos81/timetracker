console.log('Init js');

$(document).ready(function() {
   console.log('Document Ready...');

   $('.tt_stop_button').click(function() {
      var taskName = $(this).data('task_name');

      $.ajax({
         url: '/timetracker/apps/web/public/index.php/finish_task',
         data: {
            taskName:  taskName
         },
         success: function (response) {
            console.log(response);
         },
         error: function (XMLResponse) {
            console.log(XMLResponse.responseText);
         }
      });
   });
});